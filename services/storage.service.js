/** 
 * GARETH DSOUZA
 * CSE 6331 001 Spring 2018
 * Assignment 2
*/
/**
 * Handles the storage for the application.
 * Stores data both in the DB and on S3
 */
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const config = require("../config.json")
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelize = new Sequelize(config.dbConnectionString);

/* Initialize sequelize */
sequelize.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});
  

/* ========== Models needed for saving to the database ==========*/


/** 
 * Schema definition for picture table
 
 CREATE TABLE pictures
(
  id integer NOT NULL DEFAULT nextval('pictures_id_seq'::regclass),
  title character varying(255),
  path character varying(255),
  username character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  CONSTRAINT pictures_pkey PRIMARY KEY (id)
);
 * 
*/
const Picture = sequelize.define('picture', {
  title: {
    type: Sequelize.STRING
  },
  path: {
    type: Sequelize.STRING
  },
  username: {
    type: Sequelize.STRING
  }
});

/** 
 * Schema definition for picture_user_rating table
 * CREATE TABLE picture_user_ratings
(
  id integer NOT NULL DEFAULT nextval('picture_user_ratings_id_seq'::regclass),
  rating integer,
  username character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "pictureId" integer,
  CONSTRAINT picture_user_ratings_pkey PRIMARY KEY (id),
  CONSTRAINT "picture_user_ratings_pictureId_fkey" FOREIGN KEY ("pictureId")
      REFERENCES public.pictures (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL
)
 * 
*/
const PictureUserRating = sequelize.define('picture_user_rating', {
  rating: {
    type: Sequelize.INTEGER
  },
  username: {
    type: Sequelize.STRING
  }
});

Picture.hasOne(PictureUserRating, {as: 'picture'});


/* ========== Functions for various operations ==========*/


/**
 * Adds a picture entery to the table
 * @param {*} title of the picture
 * @param {*} path location of the picture
 * @param {*} username user who uploaded this photo
 */
const createPicture = function(title,path,username){
  return Picture.create({ title : title, path : path, username : username })
}

/**
 * Adds the rating given by a user for a particular picture
 * @param {*} username the user who rated this photo
 * @param {*} pictureId the id of the photo to be rated
 * @param {*} rating rating given for the photo from 1-5
 */
const addUserRatingForPicture = function(username,pictureId,rating){
  return PictureUserRating.create({ pictureId : pictureId, rating : rating, username : username })
}


/**
 * Gets all the pictures for the news feed
 */
const getAllPictures = function(){
  return new Promise (async function (resolve,reject){
    try{
      const pictureData = await Picture.findAll();
      const pictures = pictureData.map(x =>x.dataValues);
      const picturesIds = pictures.map(x =>x.id)
      const picUserRatingData = await PictureUserRating.findAll({where:{pictureId:{[Op.in]: picturesIds}}})
      const picUserRating = picUserRatingData.map(x =>x.dataValues);
      const modifiedPictures = pictures.map(p =>calRatingForPicture(p,picUserRatingData))
      resolve(modifiedPictures)
    }
    catch(err){
      reject(err)
    }
    
})
}

/**
 * Adds two params to the post object
 * 1) Rating which is 0 if there is no rating recieved.
 * 2) The list of users who have rated the picture
 * @param {*} picture the picture to be evaluted
 * @param {*} pictureUserRating a collection of picture user ratings.
 */
const calRatingForPicture = function(picture,pictureUserRating){
   const revelantPictureRatings = pictureUserRating.filter(p => p.pictureId === picture.id);
   if(revelantPictureRatings.length === 0){
    picture.rating = 0;
   }else{
    const totalRating = revelantPictureRatings.reduce((total,r) => total + r.rating, 0 )
    const value = totalRating/revelantPictureRatings.length
    picture.rating = value.toFixed(1);
  }
   picture.ratedBy = revelantPictureRatings.map(p => p.username);
   
   return picture;
}


/**
 * Uploads a stream of data to s3 and saves it.
 * @param {*} bucketName name of bucket where to store the data
 * @param {*} fileName the key of which to save the data
 * @param {*} data a readable stream of data
 * Retuns a promise.
 */
const uploadToS3 = function(bucketName,fileName,data){
  return new Promise(function (reject,resolve){
    var params = {Bucket: bucketName, Key: fileName, Body: data,ACL: 'public-read'};
    s3.upload(params, {}, function(err, data) {
      if(!err){
        reject()
      }
      resolve()
    });
  })
  
}


/* ========== Exporting the necessary functions ==========*/

module.exports.uploadToS3 = uploadToS3;
module.exports.getAllPictures = getAllPictures;
module.exports.createPicture = createPicture;
module.exports.addUserRatingForPicture = addUserRatingForPicture;
/** 
 * GARETH DSOUZA
 * CSE 6331 001 Spring 2018
 * Assignment 2
*/
/**
 * Handles the routing for the applications
 */
const express = require('express');
const fs = require('fs');
const cmd = require('node-cmd');
const router = express.Router();
const multer = require('multer');
const config = require('../config.json')
const storageService = require('../services/storage.service');
const multerupload = multer({ 'dest': 'fileupload/' });


/* Renders the homepage */
router.get('/', function(req, res) {
  res.render('index', { title: 'Home' });
});

/* Renders the page for the users to view all the posts */
router.get('/:username/posts', async function(req, res) {
  const username = req.params.username;
  const picturePosts = await storageService.getAllPictures();
  res.render('posts', { title: 'Posts',loggedInUser:username, posts:picturePosts });
});

/* Renders page for the user to create a new post */
router.get('/:username/create', function(req, res) {
  const username = req.params.username;
  res.render('create', { title: 'Upload a new photo' });
});

/**
 * POST call used to save the data for a new post.
 */
router.post('/:username/create-edit/',multerupload.any(),async function(req, res) {
  const username = req.params.username;
  const title = req.body.title
  let file = req.files[ 0 ];
  const fileName=file.originalname+"_"+new Date().getTime()
  let data = fs.createReadStream(file.path, {})
  
  //Uploads the data to s3
  try {
   await storageService.uploadToS3(config.bucketname,fileName,data)
  }
  catch(err){
    console.log("Error while uploading to S3");
    console.log(err)
  }

  //Saves the data to the DB
  try {
    await storageService.createPicture(title,config.s3link+fileName,username)
   }
  catch(err){
    console.log("Error while updating in database");
     console.log(err)
  }

  //clears temp storage
  cmd.run('rm -rf ./fileupload/*')
  res.redirect("/"+username+"/posts");
});


/**
 * GET call used to save the rating given by a user for a picture
 */
router.get('/:username/ratings/:id', async function(req, res) {
  const username = req.params.username;
  const pictureId = req.params.id;
  const rating = req.query.rating;
  try{
    await storageService.addUserRatingForPicture(username,pictureId,rating);
  }
  catch(err){
    console.log(err);
  }
  res.sendStatus(200);
});

module.exports = router;
